#include <iostream>
#include <thread>
#include <unistd.h> 
#include <string>
#include <inttypes.h>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

void (*p1)(unsigned long long int);
void (*p2)(char *);
pthread_t threads;

extern "C" void HelloWorld();
extern "C" void CreateThredCplus();
extern "C" void HelloStr(const char * value);
extern "C" int add(int a, int b);
extern "C" bool StringCsharp2Cplus( const char* value);
extern "C" const char* Getname();
extern "C" void *CallbackCplus2Csharp(void *);
extern "C" void StringCplus2Csharp(char* myString, int length)
{
    //check that length is enough for your string
  
    //simple test
    strcpy(myString, "hello Son6966");
}

void HelloWorld()
{
	std::cout << "Hello world.";
}

void CreateThredCplus()
{
    int rc;
    
    printf("CreateThredCplus\n");
    rc = pthread_create(&threads, NULL, CallbackCplus2Csharp, NULL);
    if (rc) {
        cout << "Error:unable to create thread," << rc << endl;
        exit(-1);
    }
}

void HelloStr(const char * value)
{
    
    std::cout << "Son6966 length: " << strlen(value) << std::endl;
    // string str;

    // // 24 is the size of ch
    // str.assign(value, value + strlen(value));
    // cout << str << endl;

}

int add(int a, int b)
{
    std::cout << "Son6966 a = " << a << "b = " << b << std::endl;
    return a + b;
}

bool StringCsharp2Cplus( const char* value)
{
    std::cout<< "String " << value << std::endl;
    return strlen(value) > 5;
}

const char* Getname()
{
    return "Son6966";
}
   
extern "C" void InitCallbackCplus2Csharp(void (*p)(unsigned long long int))
{
    p1 = p;
    // p(value);
    cout << "InitCallbackCplus2Csharp in lib" << endl;
}

extern "C" void InitCallbackCplus2Csharp1(void (*p)(char *))
{
    p2 = p;
    // p(value);
    cout << "InitCallbackCplus2Csharp in lib" << endl;
}

extern "C" void *CallbackCplus2Csharp(void *) 
{
    unsigned long long int data = 0x1122334455667788;
    while(true)
    {
        cout << " CallbackCplus2Csharp " << endl;
        p1(data);
        char myString[100];
        strcpy(myString, "hello Son6966 sajhdasd");
        p2(myString);
        sleep( 2 );
    }
    pthread_exit(NULL);
}
